clc
clear all
close all

%% Load the hXor landscape
addpath ../hXor

%% Compute all the genes and the landscape
for k=0:255
    Genes(k+1,:)=de2bi(k,8);
    F(k+1)=hxor(Genes(k+1,:));
end

%% Find the number of paths leading to the maximum value with the 1D decimal representations
% Find the position of the maximum 
MaxIndex=find(F==max(F));

% For every position check wheter i+1 is larger than i
Index1=F<F([2:end 1]);

% For every position check whether i-1 is larger than i
Index2=F<F([end 1:end-1]);

% Initialize the number of paths with the number of maximums
NumPaths1D=length(MaxIndex);

% For each maximum go as far to the left and the right until you find an
% upward gradient
for k=1:length(MaxIndex)
    % go to the right
    NumPaths1D=NumPaths1D+find(Index2(MaxIndex(k)+1:end)==0,1)-1;
    
    % go to the left
    NumPaths1D=NumPaths1D+find(fliplr(Index1(1:MaxIndex(k)-1)==0),1)-1;
end

%% Find the number of paths leading to the maximum value with the 1bit hill climber

% Compute the connection matrix where a point is only connected to points
% lower than itself
Change=diag(ones(1,8));
M1=zeros(256,256);
for k=1:256
    for m=1:8
        Tmp=bitxor(Genes(k,:),Change(m,:));
        if F(k)>F(bi2de(Tmp)+1)
            M1(bi2de(Tmp)+1,k)=1;
        end
    end
end

% Set the diagonal of the connection matix to 1
M1=M1-diag(diag(M1))+diag(ones(1,256));

% Initializ count
NumPaths1bit=0;

% Initialize a downward hill climbing state at the top of the landscape
State=zeros(256,1);
State(MaxIndex)=1;
% initialize an old state array that is definetly different from the
% current state
OldState=ones(256,1);

% run a while loop that registeres all the points you can reach from only
% going down from the maximum when the State vector has stabilized
while ~isequal(State,OldState)
    % save state in order to check for stability
    OldState=State;
    % Move down and force values to be binary either you have reach a point
    % or not
    State=(M1*State)>0; 
end

% The number of paths are all the reached points when the State Vector has
% stabilized
NumPaths1bit=NumPaths1bit+sum(State);



%% Find the number of paths leading to the maximum value with the Chunking hill climber
% Compute the connection matrix where a point is only connected to points
% lower than itself

% Initialize the connection matrix
M2=zeros(256,256);

% The possible moves and connections
for k=0:255
    Gene=de2bi(k,8);
    
    OP{1}=mod(Gene+ones(1,8),2);
    
    OP{2}=[mod(Gene(1:4)+ones(1,4),2) , Gene(5:8)];
    OP{3}=[Gene(1:4) mod(Gene(5:8)+ones(1,4),2)];
    OP{4}=[mod(Gene(1:2)+ones(1,2),2) Gene(3:4) Gene(5:6) Gene(7:8)];
    OP{5}=[Gene(1:2) mod(Gene(3:4)+ones(1,2),2) Gene(5:6) Gene(7:8)];
    OP{6}=[Gene(1:2) Gene(3:4) mod(Gene(5:6)+ones(1,2),2) Gene(7:8)];
    OP{7}=[Gene(1:2) Gene(3:4) Gene(5:6) mod(Gene(7:8)+ones(1,2),2)];
    OP{8}=[Gene(1:3) mod(Gene(4:5)+ones(1,2),2) Gene(6:8)];
    OP{9}=[mod(Gene(1:3)+ones(1,3),2) Gene(4:5) Gene(6:8)];
    OP{10}=[Gene(1:3) Gene(4:5) mod(Gene(6:8)+ones(1,3),2)];
    
    OP{11}=fliplr(Gene);
    
    OP{12}=[fliplr(Gene(1:4)) Gene(5:8)];
    OP{13}=[Gene(1:4) fliplr(Gene(5:8))];
    
    OP{14}=[Gene(1:3) fliplr(Gene(4:5)) Gene(6:8)];
    OP{15}=[fliplr(Gene(1:3)) Gene(4:5) Gene(6:8)];
    OP{16}=[Gene(1:3) Gene(4:5) fliplr(Gene(6:8))];
    
    
    OP{17}=[Gene(5:8),Gene(1:4)];
    OP{18}=[Gene(3:4) Gene(1:2) Gene(5:6) Gene(7:8)];
    OP{19}=[Gene(1:2) Gene(3:4) Gene(7:8) Gene(5:6)];
    OP{20}=[Gene(7:8) Gene(3:4) Gene(5:6) Gene(1:2)];
    OP{21}=[Gene(1:2) Gene(5:6) Gene(3:4) Gene(7:8)];
    OP{22}=[Gene(6:8) Gene(4:5) Gene(1:3)];
    
    OP{23}=[Gene([2 1]) Gene(3:4) Gene(5:6) Gene(7:8)];
    OP{24}=[Gene(1:2) Gene([4 3]) Gene(5:6) Gene(7:8)];
    OP{25}=[Gene(1:2) Gene(3:4) Gene([6 5]) Gene(7:8)];
    OP{26}=[Gene(1:2) Gene(3:4) Gene(5:6) Gene([8 7])];
    
    for l=1:length(OP)
        Changed=OP{l};
        Index=bi2de(Changed)+1;
        if F(k+1)>F(Index)
            M2(Index,k+1)=1;
        end
    end
end
M2=M2-diag(diag(M2))+diag(ones(1,256));

% Initializ count
NumPathsChunk=0;

% Initialize a downward hill climbing state at the top of the landscape
State=zeros(256,1);
State(MaxIndex)=1;

% initialize an old state array that is definetly different from the
% current state
OldState=ones(256,1);

% run a while loop that registeres all the points you can reach from only
% going down from the maximum when the State vector has stabilized
while ~isequal(State,OldState)
    % save state in order to check for stability
    OldState=State;
    % Move down and force values to be binary either you have reach a point
    % or not
    State=(M2*State)>0; 
end
NumPathsChunk=NumPathsChunk+sum(State);



%% Plot the figure
figure
bar([NumPaths1D NumPaths1bit NumPathsChunk])
set(gca,'xticklabel',{'BitString','1-bit flip','Chunking'})
set(gca,'plotboxaspectratio',[16,9,1])
ylabel('Points with a paths to a global maximum')
