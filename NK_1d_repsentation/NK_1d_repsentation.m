clc
clear all
close all

addpath ../NK
load F
for k=0:255
    genes=de2bi(k,8);
end

figure
plot(0:255,F)
xlim([0,255])
set(gca,'plotboxaspectratio',[16,9,1])
xlabel('BitString')
ylabel('Result')

Index1=F>F([2:end 1]);
Index2=F>F([end 1:end-1]);

NumLocalMax=sum(Index1.*Index2);

display(['Number of local maximums: ' num2str(NumLocalMax)])