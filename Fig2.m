clc
clear all
close all

FontSize = 14;
load NK/F
%% Figure 2
% open old figures
h1 = openfig('Fig2a.fig','reuse'); % open figure
ax1 = gca;
pause(0.2)
h2 = openfig('Fig2b_1.fig','reuse');
ax2 = gca;
pause(0.2)
h3 = openfig('Fig2b_2.fig','reuse');
ax3 = gca;
pause(0.2)
h4 = openfig('Fig2d.fig','reuse');
ax4 = gca;

%create new figure
h5 = figure('units','normalized','outerposition',[0.2 0.2 0.7 0.6]);
set(0,'defaultaxesfontsize',FontSize)

%create and get handle to the subplot axes
s1 = subplot(2,4,1:2); 
s2 = subplot(2,4,3);
s3 = subplot(2,4,4);
s4 = subplot(2,4,5:6);
s5 = subplot(2,4,7:8);


%get handle to all the children in the figure
fig1 = get(ax1,'children');
fig2 = get(ax2,'children');
fig3 = get(ax3,'children');
fig4 = get(ax4,'children');

%copy children to new parent axes i.e. the subplot axes
copyobj(fig1,s1);
copyobj(fig2,s2);
copyobj(fig3,s3);
copyobj(fig4,s5);


% modify Fig2a
set(s1,'visible','off')
cb = colorbar(s1);
set(cb,'Limits',[0.2,0.8])
cb.Position = cb.Position + [0.02 0 0 0];
ylim=get(s1,'ylim');
xlim=get(s1,'xlim');
set(s1,'DataAspectRatio',[1 1 1])

% modify Fig2b
set(s2,'visible','off')
ylim=get(s2,'ylim');
xlim=get(s2,'xlim');
set(s2,'DataAspectRatio',[1 1 1])

set(s3,'visible','off')
cb = colorbar(s3);
set(cb,'Limits',[0.2,0.8])
cb.Position = cb.Position + [0.02 0 0 0];
% cb.Position = cb.Position + [0.1,0,0,0];
ylim=get(s3,'ylim');
xlim=get(s3,'xlim');
set(s3,'DataAspectRatio',[1 1 1])


% modify Fig2c
plot(s4,0:255,F)
set(s4,'xlim',[0,255])
set(s4,'box','off')
child_h=get(s4,'child');
set(child_h,'linewidth',1)
xlabel(s4,'Bitstring value')
ylabel(s4,'Fitness value')
ylim=get(s4,'ylim');
xlim=get(s4,'xlim');

%modify Fig2d
set(s5,'xlim',[0.5,3.5])
set(s5,'xtick',[1:3],'xticklabel',{'BitString','1-bit flip','2-bit flip'})
set(s5,'ylim',[0,250])
ylabel(s5,'Paths leading to maximum')
ylim=get(s5,'ylim');
xlim=get(s5,'xlim');

annotation('textbox', [0.13, 0.95, 0, 0], 'string', 'a)','FontSize',FontSize)
annotation('textbox', [0.54, 0.95, 0, 0], 'string', 'b)','FontSize',FontSize)
annotation('textbox', [0.13, 0.45, 0, 0], 'string', 'c)','FontSize',FontSize)
annotation('textbox', [0.54, 0.45, 0, 0], 'string', 'd)','FontSize',FontSize)

% return

print(h5,'Fig2','-dtiff','-r500')
