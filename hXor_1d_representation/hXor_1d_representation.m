clc
clear all
close all
%% Add the hXor function to the path such that the script can call it
addpath ../hXor

%% Computer all the values in the landscape
for k=0:255
    % transform the landscape position from the decimal to the binary
    % system, while requiring the string to be 8 bits long
    genes=de2bi(k,8);
    
    % compute the value of the bitstring with the hXor function
    F(k+1)=hxor(genes);
end

%% Plot the landscape with a decimal 1d repreresentaion
figure
plot(0:255,F)
xlim([0,255])
set(gca,'plotboxaspectratio',[16,9,1])
xlabel('BitString')
ylabel('Result')