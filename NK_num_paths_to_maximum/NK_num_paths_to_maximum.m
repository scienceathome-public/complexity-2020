clc
clear all
close all

%% Load the NK landscape
addpath ../NK
load F

%% Compute all the genes
for k=0:255
    Genes(k+1,:)=de2bi(k,8);
end



%% Find the number of paths leading to the maximum value with the 1D decimal representations
% Find the position of the maximum 
MaxIndex=find(F==max(F));

% For every position check wheter i+1 is larger than i
Index1=F<F([2:end 1]);

% For every position check whether i-1 is larger than i
Index2=F<F([end 1:end-1]);

% Initialize the number of paths with the number of maximums
NumPaths1D=length(MaxIndex);

% For each maximum go as far to the left and the right until you find an
% upward gradient
for k=1:length(MaxIndex)
    % go to the right
    NumPaths1D=NumPaths1D+find(Index2(MaxIndex(k)+1:end)==0,1)-1;
    
    % go to the left
    NumPaths1D=NumPaths1D+find(fliplr(Index1(1:MaxIndex(k)-1)==0),1)-1;
end

%% Find the number of paths leading to the maximum value with the 1bit hill climber

% Compute the connection matrix where a point is only connected to points
% lower than itself
Change=diag(ones(1,8));
M1=zeros(256,256);
for k=1:256
    for m=1:8
        Tmp=bitxor(Genes(k,:),Change(m,:));
        if F(k)>F(bi2de(Tmp)+1)
            M1(bi2de(Tmp)+1,k)=1;
        end
    end
end

% Set the diagonal of the connection matix to 1
M1=M1-diag(diag(M1))+diag(ones(1,256));

% Initializ count
NumPaths1bit=0;

% Initialize a downward hill climbing state at the top of the landscape
State=zeros(256,1);
State(MaxIndex)=1;
% initialize an old state array that is definetly different from the
% current state
OldState=ones(256,1);

% run a while loop that registeres all the points you can reach from only
% going down from the maximum when the State vector has stabilized
while ~isequal(State,OldState)
    % save state in order to check for stability
    OldState=State;
    % Move down and force values to be binary either you have reach a point
    % or not
    State=(M1*State)>0; 
end

% The number of paths are all the reached points when the State Vector has
% stabilized
NumPaths1bit=NumPaths1bit+sum(State);


%% Find the number of paths leading to the maximum value with the 2bit hill climber
% Compute the connection matrix where a point is only connected to points
% lower than itself

% Initialize the connection matrix
M2=zeros(256,256);

% The possible moves
Change=[];
for k=1:7
    for m=(k+1):8
        NewString=zeros(1,8);
        NewString(k)=1;
        NewString(m)=1;
        Change=[Change;NewString];
    end
end

% Find the connections
for k=0:255
    Gene= Genes(k+1,:);
    for l=1:size(Change,1)
        Changed=bitxor(Gene,Change(l,:));
        Index=bi2de(Changed)+1;
        M2(Index,k+1)=1;
    end
end
M2=M2-diag(diag(M2))+diag(ones(1,256));

% Initializ count
NumPaths2Bit=0;

% Initialize a downward hill climbing state at the top of the landscape
State=zeros(256,1);
State(MaxIndex)=1;

% initialize an old state array that is definetly different from the
% current state
OldState=ones(256,1);

% run a while loop that registeres all the points you can reach from only
% going down from the maximum when the State vector has stabilized
while ~isequal(State,OldState)
    % save state in order to check for stability
    OldState=State;
    % Move down and force values to be binary either you have reach a point
    % or not
    State=(M2*State)>0; 
end
NumPaths2Bit=NumPaths2Bit+sum(State);

%% Plot the figure
figure
bar([NumPaths1D NumPaths1bit NumPaths2Bit])
set(gca,'xticklabel',{'BitString','1-bit flip','2-bit flip'})
set(gca,'plotboxaspectratio',[16,9,1])
ylabel('Points with a paths to a global maximum')
