clc
clear all
close all
%% Load external function
addpath ../tSNE % tSNE function
addpath ../NK   % path to the NK landscape
load F          % pre computed NK landscape


%% Computer all the positions in the landscape in the form of bitstrings
% initialize storage of the positions in the landscape called genes
genes = zeros(256,8);

for k=0:255
    % transform the landscape position from the decimal to the binary
    % system, while requiring the string to be 8 bits long
    genes(k+1,:)=de2bi(k,8);
    
end

%% Compute all the possible movew where two bits are changed at the same time
Change=[];
for k=1:7
    for m=(k+1):8
        NewString=zeros(1,8);
        NewString(k)=1;
        NewString(m)=1;
        Change=[Change;NewString];
    end
end

%% Compute the connection matrix between the positions in the landscape when
% using the chunking algorithm

% initialize connection matrix
M=zeros(256,256);
for k=0:255
    % the base gene to look at right now
    Gene=genes(k+1,:);
    
    % apply all the moves and find the neighbours
    for l=1:size(Change,1)
        Changed=bitxor(Gene,Change(l,:));
        Index=bi2de(Changed)+1;
        M(Index,k+1)=1;
    end
end

%% Calcualte distance between all the points in the landscape
% initialize distance matrix
Distance=zeros(256,256);

% the distance between points can be found by moving from all the neighburs
% of the points you have previousely been able to reach. 
for k=1:256
    Tmp=zeros(256,1);
    Tmp(k)=1;
    Index=zeros(256,1);
    m=0;
    while sum(Index([1:k-1 k+1:end])>0)<255
        m=m+1;
        Tmp=(M*Tmp)>0;
        TmpIndex=Index==0;
        TmpIndex(k)=0;
        Index(TmpIndex)=Tmp(TmpIndex)*m;
    end
    Distance(:,k)=Index;
end

%% Plot the landscape

% find the two pathhes of the landscape that are not connected by any
% series of two bit flips
Index=Distance(:,1)==0;
Distance1=Distance;
Distance1(Index,:)=[];
Distance1(:,Index)=[];

Distance2=Distance;
Distance2(Index==0,:)=[];
Distance2(:,Index==0)=[];

% compute positions in the 8 dimensional landscape in a two the plane
% using the tsne algorithm
mappedX1=tsne_d(Distance1,[],2,20);
mappedX2=tsne_d(Distance2,[],2,20);

% plot the figure
figure
x=mappedX1(:,1);
y=mappedX1(:,2);
z=F(Index==0)';
tri = delaunay(x,y);
h = trisurf(tri, x, y, z,'FaceColor', 'interp');
view(2)
set(gca,'PlotBoxAspectratio',[1,1,1])
axis off
shading interp
set(h,'EdgeColor', 'none')
caxis([min(F) max(F)])

figure
x=mappedX2(:,1);
y=mappedX2(:,2);
z=F(Index)';
tri = delaunay(x,y);
h = trisurf(tri, x, y, z,'FaceColor', 'interp');
view(2)
set(gca,'PlotBoxAspectratio',[1,1,1])
axis off
cb=colorbar;
cb.Position = cb.Position + [0.1 0 0 0];

shading interp
set(h,'EdgeColor', 'none')
caxis([min(F) max(F)])


