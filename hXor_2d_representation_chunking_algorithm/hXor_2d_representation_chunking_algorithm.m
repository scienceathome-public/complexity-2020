clc
clear all
close all
%% Load external function
addpath ../hXor % hxor function
addpath ../tSNE % tSNE function

%% Computer all the values in the landscape
% initialize storage of the positions in the landscape called genes
genes = zeros(256,8);

for k=0:255
    % transform the landscape position from the decimal to the binary
    % system, while requiring the string to be 8 bits long
    genes(k+1,:)=de2bi(k,8);
    
    % compute the value of the bitstring with the hXor function
    F(k+1)=hxor(genes(k,:));
end

%% Compute the connection matrix between the positions in the landscape when
% using the chunking algorithm

% initialize connection matrix
M=zeros(256,256);

for k=0:255
    % the base gene to look at right now
    Gene=genes(k+1,:);
    
    % invert all bits
    OP{1}=mod(Gene+ones(1,8),2);
    
    % invert half the bits
    OP{2}=[mod(Gene(1:4)+ones(1,4),2) , Gene(5:8)];
    OP{3}=[Gene(1:4) mod(Gene(5:8)+ones(1,4),2)];
    
    % invert two neighbouring bits
    OP{4}=[mod(Gene(1:2)+ones(1,2),2) Gene(3:4) Gene(5:6) Gene(7:8)];
    OP{5}=[Gene(1:2) mod(Gene(3:4)+ones(1,2),2) Gene(5:6) Gene(7:8)];
    OP{6}=[Gene(1:2) Gene(3:4) mod(Gene(5:6)+ones(1,2),2) Gene(7:8)];
    OP{7}=[Gene(1:2) Gene(3:4) Gene(5:6) mod(Gene(7:8)+ones(1,2),2)];
    
    % invert the center bits
    OP{8}=[Gene(1:3) mod(Gene(4:5)+ones(1,2),2) Gene(6:8)];
    
    % invert the outer 3 bits
    OP{9}=[mod(Gene(1:3)+ones(1,3),2) Gene(4:5) Gene(6:8)];
    OP{10}=[Gene(1:3) Gene(4:5) mod(Gene(6:8)+ones(1,3),2)];
    
    % mirror the bitstring
    OP{11}=fliplr(Gene);
    
    % mirror the left or the right half of the bitstring
    OP{12}=[fliplr(Gene(1:4)) Gene(5:8)];
    OP{13}=[Gene(1:4) fliplr(Gene(5:8))];
    
    % mirror the center bits
    OP{14}=[Gene(1:3) fliplr(Gene(4:5)) Gene(6:8)];
    
    % mirror the outer 3 bits
    OP{15}=[fliplr(Gene(1:3)) Gene(4:5) Gene(6:8)];
    OP{16}=[Gene(1:3) Gene(4:5) fliplr(Gene(6:8))];

    % switch the left and the right half
    OP{17}=[Gene(5:8),Gene(1:4)];
    
    % switch two 2-bit blocks
    OP{18}=[Gene(3:4) Gene(1:2) Gene(5:6) Gene(7:8)];
    OP{19}=[Gene(1:2) Gene(3:4) Gene(7:8) Gene(5:6)];
    OP{20}=[Gene(7:8) Gene(3:4) Gene(5:6) Gene(1:2)];
    OP{21}=[Gene(1:2) Gene(5:6) Gene(3:4) Gene(7:8)];
    
    % switch the two outher 3-bits
    OP{22}=[Gene(6:8) Gene(4:5) Gene(1:3)];
    
    % switch two bits within a 2-bit block
    OP{23}=[Gene([2 1]) Gene(3:4) Gene(5:6) Gene(7:8)];
    OP{24}=[Gene(1:2) Gene([4 3]) Gene(5:6) Gene(7:8)];
    OP{25}=[Gene(1:2) Gene(3:4) Gene([6 5]) Gene(7:8)];
    OP{26}=[Gene(1:2) Gene(3:4) Gene(5:6) Gene([8 7])];
    
    % record the changed bitstrings into the connection matrix
    for l=1:length(OP)
        Changed=OP{l};
        Index=bi2de(Changed)+1;
        M(Index,k+1)=1;
    end
end

% remove the diagonal of the connection matrix
M=M-diag(diag(M));


%% Calcualte distance between all the points in the landscape
% initialize distance matrix
Distance=zeros(256,256);

% the distance between points can be found by moving from all the neighburs
% of the points you have previousely been able to reach. 
for k=1:256
    Tmp=zeros(256,1);
    Tmp(k)=1;
    Index=zeros(256,1);
    m=0;
    while sum(Index([1:k-1 k+1:end])>0)<255
        m=m+1;
        Tmp=(M*Tmp)>0;
        TmpIndex=Index==0;
        TmpIndex(k)=0;
        Index(TmpIndex)=Tmp(TmpIndex)*m;
    end
    Distance(:,k)=Index;
end


%% Plot the landscape
% compute positions in the 8 dimensional landscape in a two the plane
% using the tsne algorithm
mappedX=tsne_d(Distance,[],2,26);


% plot the figure
figure
x=mappedX(:,1);
y=mappedX(:,2);
z=F;
tri = delaunay(x,y);
h = trisurf(tri, x, y, z,'FaceColor', 'interp');
view(2)
set(gca,'PlotBoxAspectratio',[1,1,1])
axis off
colorbar
shading interp
set(h,'EdgeColor', 'none')
