clc
clear all
close all
%% Load external function
addpath ../hXor % hxor function
addpath ../tSNE % tSNE function


%% Computer all the values in the landscape
% initialize storage of the positions in the landscape called genes
genes = zeros(256,8);

for k=0:255
    % transform the landscape position from the decimal to the binary
    % system, while requiring the string to be 8 bits long
    genes(k+1,:)=de2bi(k,8);
    
    % compute the value of the bitstring with the hXor function
    F(k+1)=hxor(genes(k,:));
end

%% Calcualte distance between all the points in the landscape
% initialize distance matrix
Distance=zeros(256,256);

% compute the 1 bit distance between all pair of positions
for k=1:256
    for m=k+1:256
        Distance(k,m)=sum(abs(genes(k,:)-genes(m,:)));
    end
end

% Since only the upper triangle of the distance matrix has been computed
% add the lower triangle, which is the transposed distance matrix
Distance=Distance+Distance';

%% Plot the landscape
% compute positions in the 8 dimensional landscape in a two the plane
% using the tsne algorithm
mappedX=tsne_d(Distance,[],2,8);

% plot the figure
figure
x=mappedX(:,1);
y=mappedX(:,2);
z=F;
tri = delaunay(x,y);
h = trisurf(tri, x, y, z,'FaceColor', 'interp');
view(2)
set(gca,'PlotBoxAspectratio',[1,1,1])
axis off
colorbar
shading interp
set(h,'EdgeColor', 'none')