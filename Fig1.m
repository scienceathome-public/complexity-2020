clc
clear all
close all

FontSize = 14;

h5 = figure('units','normalized','outerposition',[0.2 0.2 0.6 0.6]);
set(h5,'defaultaxesfontsize',FontSize)
x = linspace(-3,3,200);

subplot(2,2,1)
y = exp(-x.^2);
plot(x,y,'linewidth',1)
box off
set(gca,'xtick',[],'ytick',[])
set(gca,'xlim',[-3,3])
% xlabel('Position')
% ylabel('Fitness value')
ylim=get(gca,'ylim');
xlim=get(gca,'xlim');
text((xlim(2)-xlim(1))*0.02+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'a)','FontSize',FontSize)

subplot(2,2,2)
y = exp(-x.^2)+0.1*sin(10*x);
plot(x,y,'linewidth',1)
box off
set(gca,'xtick',[],'ytick',[])
set(gca,'xlim',[-3,3])
% xlabel('Position')
% ylabel('Fitness value')
ylim=get(gca,'ylim');
xlim=get(gca,'xlim');
text((xlim(2)-xlim(1))*0.02+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'b)','FontSize',FontSize)

subplot(2,2,3)
y = exp(-x.^2)+0.9*exp(-(x-2).^2/0.5);
plot(x,y,'linewidth',1)
box off
set(gca,'xtick',[],'ytick',[])
set(gca,'xlim',[-3,3])
% xlabel('Position')
% ylabel('Fitness value')
ylim=get(gca,'ylim');
xlim=get(gca,'xlim');
text((xlim(2)-xlim(1))*0.02+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'c)','FontSize',FontSize)

subplot(2,2,4)
y = exp(-x.^2)+0.9*exp(-(x-2).^2/0.5);
y(150:end) = y(150);
plot(x,y,'linewidth',1)
box off
set(gca,'xtick',[],'ytick',[])
set(gca,'xlim',[-3,3])
% xlabel('Position')
% ylabel('Fitness value')
ylim=get(gca,'ylim');
xlim=get(gca,'xlim');
text((xlim(2)-xlim(1))*0.02+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'d)','FontSize',FontSize)


print(h5,'Fig1','-dtiff','-r500')