%% Function that returns the hxor value of a bitstring
function result=hxor(x)


Length=length(x);

% it the length of the bitstring is 1 return 1 else compute the hxor value
if Length>1
    
    % split the bitstring into the left and the right part.
    L=x(1:Length/2);
    R=x((Length/2+1):end);
    
    % return the hxor value of the bitstring.
    result=hxor(L)+hxor(R)+h(x)*Length;
    
else
    result=1;
end
end

%% subfunction for the hxor function
function result=h(x)
% how long is the bitstring
Length=length(x);

% it the length of the bitstring is 1 return 1 else go into 
if Length==1
    result=1;
else
    
    % split the bitstring into the left and the right part.
    L=x(1:Length/2);
    R=x((Length/2+1):end);
    
    % call the subfunction on the left and the right strings
    L_result=h(L);
    R_result=h(R);
    
    % check whether the results of the function calls to the subfunctions
    % are equal
    Check1 = L_result==R_result;
    
    % do pitwise negation on the right bitstring and check whether the
    % resulting string and the left bitstring are equal
    Check2 = isequal(L,R==0);
    
    % If both the checks are equal return 1 otherwise return 0
    if Check1 && Check2
        result=1;
    else
        result=0;
    end
end
end
