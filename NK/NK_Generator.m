%% NK landscape generator function
% N is given by the length of the bitstring given to the function
% Inputs:
%           gene:       bitstring representation of the position
%           k:          the K parameter in the NK landscape
%           oldValues:  already chosen values for interelated values

function fitness=NK_Generator(gene,k,oldValues)

% set the base fitness value of the given gene to 0
fitness=0;

% length of the gene is N
Length=length(gene);

% Check whether this is a new landscape and initialize the interaction
% values, else use the already initialized array and set values
if isempty(oldValues)
    oldValues=cell(Length,2^(k+1));
end

% Generate all the interaction blocks of length K+1, by taking the next K
% elements of the 1 to N array in a circualar manner
% E.g. with N = 8, and K = 3
% List =[1     2     3     4;
%        2     3     4     5;
%        3     4     5     6;
%        4     5     6     7;
%        5     6     7     8;
%        6     7     8     1;
%        7     8     1     2;
%        8     1     2     3]
Index=1:Length;
for l=1:Length
    if l+k<=Length
        % take the straight forward block of indices
        List(l,:)=[Index(l:l+k)];
    else
        % take the indices in a circular mannaer
        List(l,:)=[Index(l:Length) Index(1:(k+1-(Length-l+1)))];
    end
end

% Run over each interactions and generate values
for l=1:Length
    % turn the subgene into a decimal number
    Index=bi2de(gene(List(l,:)))+1;
    
    % check the array of old values
    TmpVal=oldValues{l,Index};
    
    % If the old value is empty draw a random number from a uniform
    % distribution [0,1], otherwice use the old value
    if isempty(TmpVal)
        Val(l)=rand(1);
        oldValues{l,Index}=Val(l);
    else
        Val(l)=TmpVal;
    end    
end

% the fitness value is the average of the old values
fitness=mean(Val);

% save the old values to a file
save oldValues oldValues
end