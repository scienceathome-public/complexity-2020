clc
clear all
close all

FontSize = 14;

%% Figure 3
% open old figures
h1 = openfig('Fig3a.fig','reuse'); % open figure
ax1 = gca;
pause(0.3)
h2 = openfig('Fig3b.fig','reuse');
ax2 = gca;
pause(0.3)
h3 = openfig('Fig3c.fig','reuse');
ax3 = gca;
pause(0.3)
h4 = openfig('Fig3d.fig','reuse');
ax4 = gca;

%create new figure
h5 = figure('units','normalized','outerposition',[0.2 0.2 0.6 0.6]);
set(0,'defaultaxesfontsize',FontSize)

%create and get handle to the subplot axes
s1 = subplot(2,2,1); 
s2 = subplot(2,2,2);
s3 = subplot(2,2,3);
s4 = subplot(2,2,4);

%get handle to all the children in the figure
fig1 = get(ax1,'children');
pause(0.1)
fig2 = get(ax2,'children');
pause(0.1)
fig3 = get(ax3,'children');
pause(0.1)
fig4 = get(ax4,'children');

%copy children to new parent axes i.e. the subplot axes
copyobj(fig1,s1);
pause(0.1)
copyobj(fig2,s2);
pause(0.1)
copyobj(fig3,s3);
pause(0.1)
copyobj(fig4,s4);
pause(0.1)

% modify Fig3a
set(s1,'visible','off')
colorbar(s1)
ylim=get(s1,'ylim');
xlim=get(s1,'xlim');
% text(s1,(xlim(2)-xlim(1))*0.05+xlim(1),(ylim(2)-ylim(1))*0.95+ylim(1),'a)','FontSize',FontSize)
set(s1,'DataAspectRatio',[1 1 1])

% modify Fig3b
set(s2,'visible','off')
colorbar(s2)
ylim=get(s2,'ylim');
xlim=get(s2,'xlim');
% text(s2,(xlim(2)-xlim(1))*0.05+xlim(1),(ylim(2)-ylim(1))*0.95+ylim(1),'b)','FontSize',FontSize)
set(s2,'DataAspectRatio',[1 1 1])

% modify Fig3c
set(s3,'xlim',[0,255])
child_h=get(s3,'child');
set(child_h,'linewidth',1)
xlabel(s3,'Bitstring value')
ylabel(s3,'Fitness value')
ylim=get(s3,'ylim');
xlim=get(s3,'xlim');
% text(s3,(xlim(2)-xlim(1))*0.05+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'c)','FontSize',FontSize)

% modify Fig3d
set(s4,'xlim',[0.5,3.5])
set(s4,'xtick',[1,2,3],'xticklabel',{'Bitstring','1-bit flip','Chunking'})
set(s4,'ylim',[0,250])
ylabel(s4,'Paths leading to maximum')
ylim=get(s4,'ylim');
xlim=get(s4,'xlim');
% text(s4,(xlim(2)-xlim(1))*0.05+xlim(1),(ylim(2)-ylim(1))*0.90+ylim(1),'d)','FontSize',FontSize)

% set(findall(h5,'-property','FontSize'),'FontSize',16)

annotation('textbox', [0.13, 0.95, 0, 0], 'string', 'a)','FontSize',FontSize)
annotation('textbox', [0.57, 0.95, 0, 0], 'string', 'b)','FontSize',FontSize)
annotation('textbox', [0.13, 0.45, 0, 0], 'string', 'c)','FontSize',FontSize)
annotation('textbox', [0.57, 0.45, 0, 0], 'string', 'd)','FontSize',FontSize)

print(h5,'Fig3','-dtiff','-r500')
